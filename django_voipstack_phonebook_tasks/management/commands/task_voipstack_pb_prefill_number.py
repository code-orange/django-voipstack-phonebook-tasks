from django.core.management.base import BaseCommand

from django_voipstack_phonebook_tasks.django_voipstack_phonebook_tasks.tasks import (
    voipstack_pb_prefill_number,
)


class Command(BaseCommand):
    help = "Run task: voipstack_pb_prefill_number"

    def handle(self, *args, **options):
        voipstack_pb_prefill_number()
