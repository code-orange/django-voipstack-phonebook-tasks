import csv
import shutil
import tempfile
from datetime import date
from io import StringIO

from celery import shared_task
from django.conf import settings

from django_mdat_phone.django_mdat_phone.models import (
    MdatPhoneNationalCode,
    MdatPhoneCountryCode,
)
from django_voipstack_phonebook.django_voipstack_phonebook.models import *


@shared_task(name="voipstack_pb_prefill_number")
def voipstack_pb_prefill_number():
    numbers = VoipMdatNumbers.objects.filter(status=VoipMdatNumbers.NUM_STATUS_ACTIVE)

    for number in numbers:
        phonebook_data_rows = VoipPhonebookData.objects.filter(number=number)

        if len(phonebook_data_rows) > 0:
            continue

        phonebook_data = VoipPhonebookData()

        phonebook_data.number = number
        phonebook_data.name = number.customer.name
        phonebook_data.street = "undefined"
        phonebook_data.house_nr = "0"

        # Country
        country = (
            MdatPhoneCountryCode.objects.filter(country_code=number.country_code)
            .first()
            .country
        )

        # National Code
        national_code = MdatPhoneNationalCode.objects.filter(
            city__country=country,
            national_code=number.national_code,
        ).first()

        phonebook_data.zipcode = national_code.city.zip_code
        phonebook_data.city = national_code.city.title

        phonebook_data.number_type_id = 2

        phonebook_data.save()

    return


@shared_task(name="voipstack_pb_deteme_sync_out")
def voipstack_pb_deteme_sync_out():
    now = date.today()
    partner = VoipPhonebookPartner.objects.get(id=1)
    sequence = partner.sequence + 1

    filename = (
        settings.PHONEBOOK_SYNC_DTM_IDENT
        + "_"
        + str(sequence).zfill(4)
        + "_"
        + now.strftime("%Y%m%d")
        + ".txt"
    )

    phonebook_transfer_file = StringIO(newline="")

    phonebook_transfer_csv = csv.writer(
        phonebook_transfer_file, delimiter="\t", quotechar=None, quoting=csv.QUOTE_NONE
    )

    all_phonebook_entries = VoipPhonebookData.objects.all()

    phonebook_transfer_csv.writerow(
        [
            "CV01.2",  # V00 - API version
            settings.PHONEBOOK_SYNC_DTM_IDENT,  # V01 - Own Ident
            "",  # V02 - Serial, not used
            now.strftime("%d.%m.%Y"),  # V03 - Delivery date
            sequence,  # V04 - Delivery serial
            "",  # V05 - Received date, filled by DTM
            "",  # V06 - Return date, filled by DTM
            str(len(all_phonebook_entries)),  # V07 - Row count
            "",  # V08 - Return code, filled by DTM
        ]
    )

    for entry in all_phonebook_entries:
        row_data = list()

        try:
            foreign_id = entry.voipphonebookpartnerdeteme_set.get()
        except VoipPhonebookPartnerDeteme.DoesNotExist:
            foreign_id = ""
            action = "A"
        else:
            foreign_id = str(foreign_id.data_id)
            action = "E"

        # single / block number
        if entry.number.num_type == VoipMdatNumbers.NUM_TYPE_BLOCK:
            range_default = str(entry.number.range_default)
        else:
            range_default = ""

        row_data.append(settings.PHONEBOOK_SYNC_DTM_CUST_NR)  # A01 - Kundennummer
        row_data.append(str(entry.id))  # A02 - Fremd-ID
        row_data.append(foreign_id)  # A03 - Datensatz-ID
        row_data.append("")  # A04 - Gültig von
        row_data.append("")  # A05 - Gültig bis
        row_data.append(action)  # A06 - Auftragsart
        row_data.append("")  # A07 - Bearbeitungsdatum) filled by DTM
        row_data.append("")  # A08 - Return Code) filled by DTM
        row_data.append("")  # A09 - nicht belegt
        row_data.append("")  # R01 - Netz-ID) not used
        row_data.append("00" + str(entry.number.country_code))  # R02 - Länderkennung
        row_data.append("0" + str(entry.number.national_code))  # R03 - Ortsnetzkennzahl
        row_data.append(str(entry.number.subscriber_num))  # R04 - Rufnummer
        row_data.append(range_default)  # R05 - Zentrale
        row_data.append("")  # R06 - Nebenstelle
        row_data.append("")  # K01 - nicht belegt
        row_data.append("")  # K02 - nicht belegt
        row_data.append("")  # K03 - nicht belegt
        row_data.append("")  # K04 - nicht belegt
        row_data.append("")  # K05 - nicht belegt
        row_data.append("")  # K06 - nicht belegt
        row_data.append("")  # K07 - nicht belegt
        row_data.append("")  # K08 - nicht belegt
        row_data.append("")  # K09 - nicht belegt
        row_data.append("")  # K10 - nicht belegt
        row_data.append("")  # K11 - nicht belegt
        row_data.append("")  # K12 - nicht belegt
        row_data.append("")  # K13 - nicht belegt
        row_data.append("")  # K14 - nicht belegt
        row_data.append("")  # K15 - nicht belegt
        row_data.append("")  # K16 - nicht belegt

        if entry.company_description is not None:
            row_data.append(
                entry.company_description.value
            )  # E01 - Berufs/Geschäftsbeteichnung
        else:
            row_data.append("")

        if entry.usage is not None:
            row_data.append(entry.usage.value)  # E02 - Anschlussnutzung
        else:
            row_data.append("")

        row_data.append("")  # E03 - nicht belegt

        if entry.print_allowed is not None:
            row_data.append(entry.print_allowed.value)  # E04 - Datennutzung Print
        else:
            row_data.append("")

        if entry.digital_allowed is not None:
            row_data.append(
                entry.digital_allowed.value
            )  # E05 - Datennutzung Elektronik
        else:
            row_data.append("")

        if entry.hotline_allowed is not None:
            row_data.append(entry.hotline_allowed.value)  # E06 - Datennutzung Auskunft
        else:
            row_data.append("")

        row_data.append(entry.name)  # E07 - Name
        row_data.append(entry.firstname)  # E08 - Vorname

        if entry.hist_name is not None:
            row_data.append(entry.hist_name.value)  # E09 - Historischer namenszusatz
        else:
            row_data.append("")

        if entry.prefix_lastname is not None:
            row_data.append(entry.prefix_lastname.value)  # E10 - Vorsatzwort
        else:
            row_data.append("")

        if entry.name_title is not None:
            row_data.append(entry.name_title)  # E11 - Titel
        else:
            row_data.append("")

        row_data.append(entry.street)  # E12 - Straße
        row_data.append(entry.house_nr)  # E13 - Hausnummer

        if entry.house_add is not None:
            row_data.append(entry.house_add)  # E14 - hausnummernzusatz
        else:
            row_data.append("")

        row_data.append(entry.zipcode)  # E15 - PLZ
        row_data.append(entry.city)  # E16 - Ort

        if entry.city_add is not None:
            row_data.append(entry.city_add)  # E17 - Ortsnamenszusatz
        else:
            row_data.append("")

        row_data.append("")  # E18 - nicht belegt
        row_data.append(entry.number_type.value)  # E19 - Suchverzeichnis

        if entry.inverse_search_allowed:
            row_data.append("J")  # E20 - Inverse_suche
        else:
            row_data.append("N")  # E20 - Inverse_suche

        row_data.append("")  # E21 - nicht belegt
        row_data.append("")  # E22 - Veröffentlichungs-KZ-Adresse
        row_data.append("")  # E23 - nicht belegt
        row_data.append("")  # E24 - nicht belegt
        row_data.append("")  # E25 - nicht belegt

        if entry.tag is not None:
            row_data.append(entry.tag.value)  # E26 - Stichwort
        else:
            row_data.append("")

        row_data.append("")  # E27 - Komplex-KZ

        if entry.number_blocked:
            row_data.append("x")  # E28 - Unterdrückung Rufnummern-Anzeige
        else:
            row_data.append("")  # E28 - Unterdrückung Rufnummern-Anzeige

        if entry.name_add is not None:
            row_data.append(entry.name_add)  # E29 - sonstiger Namenszusatz
        else:
            row_data.append("")

        row_data.append("")  # E30 - nicht belegt
        row_data.append("")  # E31 - nicht belegt

        phonebook_transfer_csv.writerow(row_data)

    temp_path = tempfile.mkdtemp()

    with open(temp_path + "/" + filename, "wb") as export_file:
        phonebook_transfer_file.seek(0)
        export_file.write(phonebook_transfer_file.getvalue().encode("iso8859-15"))

    shutil.rmtree(temp_path)

    partner.sequence = sequence
    partner.save()

    return
